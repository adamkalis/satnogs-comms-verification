**Description**

*Add description of test case*

Example 1:
Measure and verify that COMMS on S-band complies with SFCG 21–2R4 recommendation spectrum mask and the total power contained in any single spurious emission does not exceed -60 dBc

**Requirements**

*Requirements that are tested/related with this test case*

Example 1:
https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-org/-/issues/32

**Equipment**

*Describe necessary equipment to perform the test case*

Example 1:
* Spectrum Analyzer
* MMCX to SMA Cable
* Test PC
* USB to CAN Device
* Bench Power Supply
* Power Cable

**Sequence**

*Describe all the necessary steps to perform the test case*

Example 1:
1. Set up spectrum analyzer frequency at 435MHz, full span and maximum hold setting
2. Connect the COMMS with spectrum analyzer by using MMCX to SMA cable
3. Connect COMMS CAN port with test PC USB port via USB to CAN Device
4. Power ON the COMMS by using Bench Power Supply with 12V and 2A
5. Initialize command line interface in Test PC
6. Send configuration setup command to COMMS via command line interface in Test PC
7. Send TX S-Band command to COMMS via command line interface in Test PC

**Expected Result**

*Add expected result of test case*

Example 1:
Spectrum analyzer measurements of TX signal follows the SFCG 21–2R4 recommendation spectrum mask. The total power contained in any single spurious emission does not exceed -60 dBc

/label ~"Test Case"
